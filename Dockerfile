FROM ubuntu:22.04
RUN apt update 
RUN apt install python3 -y
RUN apt install python3-pip -y
RUN pip3 install requests
RUN pip3 install subprocess.run
RUN pip3 install flask 
RUN pip3 install sp 
WORKDIR /home/ubuntu
COPY test.py .
COPY dd.py .
RUN chmod +x test.py
RUN chmod +x dd.py
EXPOSE 80
ENTRYPOINT [ "python3" ]
CMD [ "test.py" ]
